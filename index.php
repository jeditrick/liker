<?php
session_start();
use GuzzleHttp\Client;

require 'vendor/autoload.php';

$app = new \Slim\Slim();
Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem('src/views');
$twig = new Twig_Environment($loader);

$app->get('/', function () use ($app) {
    $r = 'https://oauth.vk.com/authorize?client_id=5025616&redirect_uri=http://127.0.0.1/liker/get_token&display=page&response_type=code';
    $app->redirect($r);
});

$app->get('/get_token', function () use ($app) {
    $code = $_GET['code'];
    $client = new Client();
    $r = 'https://oauth.vk.com/access_token?client_id=5025616&client_secret=DcK9VtCokCjjP06oUxfS&redirect_uri=http://127.0.0.1/liker/get_token&code=' . $code;
    $r = json_decode($client->get($r)->getBody(), 1);
    $_SESSION['access_token'] = $r['access_token'];
    $app->redirect('home');
});

$app->get('/home', function () use ($twig) {
    $template = $twig->loadTemplate('home.php');
    echo $template->render(array('the' => 'variables', 'go' => 'here'));
});



$app->post('/get_likes', function ()  {
    $link = $_POST['link'];
    $token = $_SESSION['access_token'];

});

$app->run();