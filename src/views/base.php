<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liker</title>
    <link rel = "stylesheet" href = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel = "stylesheet" href = "../../resources/css/main.css">
    <style>
        input{
            font-weight: bold;
        }
        #content{
            margin: 30px;
        }
    </style>
</head>
<body>
<div id = "content">{% block content %}{% endblock %}</div>
<div id = "footer">
</div>
<script src = "//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src = "../../resources/js/main.js"></script>
</body>
</html>