<?php
namespace Kolya;


class Main
{
    private $token;
    private $vkLink;
    private $vkId;

    public function __construct($token, $vk_link)
    {
        $this->token = $token;
        if ($this->checkVkLink($vk_link)) {
            $this->vkLink = $vk_link;
            $this->vkId = $this->getVkId($this->vkLink);
        } else {
            throw new \Exception('Bad VK link');
        }
    }

    private function checkVkLink($link)
    {
        return preg_match('/(http(s)?:\/\/)?vk.com\/(\d+|\w+).(\d+|\w+)/',$link);
    }

    private function getVkId($link)
    {

        return false;
    }

}
